<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.config
 */
/**
 * Classe clsConfigBD
 * <pre>
 * Classe usada para manipular os arquivos de configuraçao dos bancos de dados
 * </pre>
 */
class clsConfigBD
{
    /**
     * @var string Armazena o nome do arquivo que será manipulado
     * @access private
     */
    private $arquivo;

    /**
     * @var array Armazena as configurações do arquivo
     * @access private
     */
    private $configuracoes;

    /**
     * Método construtor
     * <pre>
     * Define o arquivo que será manipulado na hora da instancia do objeto
     * </pre>
     * @param string $arquivo Nome do arquivo que será manipulado pelo objeto
     */
    public function  __construct($arquivo)
    {
        //Seta a localização, o nome e a extenção INI para arquivo de configuração
        $this->arquivo = "/var/www/intravip/trunk/sys/app.config/".$arquivo.".ini";
    }

    /**
     * Método setConfig()
     * <pre>
     * Método usado para modificar as informações contidas no arquivo de configuraçao
     * </pre>
     * @param string $host Endereço do host do banco
     * @param string $port Porta usada na conexão
     * @param string $name Nome do banco de dados
     * @param string $user Usuário do banco de dados
     * @param string $pass Senha do banco de dados
     * @param string $type Driver usado na conexão
     */
    public function setConfig($host = null, $port = null, $name = null, $user = null, $pass = null, $type = null)
    {
        //Verifica se o arquivo existe
        if(file_exists($this->arquivo))
        {
            //Carrega as configurações do arquivo
            $this->configuracoes = parse_ini_file($this->arquivo);

            if($host)
            {
                $this->configuracoes['host'] = $host;
            }
            if($port)
            {
                $this->configuracoes['port'] = $port;
            }
            if($name)
            {
                $this->configuracoes['name'] = $name;
            }
            if($user)
            {
                $this->configuracoes['user'] = $user;
            }
            if($pass)
            {
                $this->configuracoes['pass'] = $pass;
            }
            if($type)
            {
                $this->configuracoes['type'] = $type;
            }

            try
            {
                $texto  = "host = {$this->configuracoes['host']}\n";
                $texto .= "port = {$this->configuracoes['port']}\n";
                $texto .= "name = {$this->configuracoes['name']}\n";
                $texto .= "user = {$this->configuracoes['user']}\n";
                $texto .= "pass = {$this->configuracoes['pass']}\n";
                $texto .= "type = {$this->configuracoes['type']}\n";

                $handle = fopen($this->arquivo, 'w');
                fwrite($handle, $texto);
                fclose($handle);
            }

            catch (Exception $e)
            {
                echo $e->getMessage();
            }

        }
        //Se não existir, cria o arquivo com as configurações passadas
        else
        {
            if($host && $port && $name && $user && $pass && $type)
            {
                $this->configuracoes['host'] = $host;
                $this->configuracoes['port'] = $port;
                $this->configuracoes['name'] = $name;
                $this->configuracoes['user'] = $user;
                $this->configuracoes['pass'] = $pass;
                $this->configuracoes['type'] = $type;

                try
                {
                    $texto  = "host = {$this->configuracoes['host']}\n";
                    $texto .= "port = {$this->configuracoes['port']}\n";
                    $texto .= "name = {$this->configuracoes['name']}\n";
                    $texto .= "user = {$this->configuracoes['user']}\n";
                    $texto .= "pass = {$this->configuracoes['pass']}\n";
                    $texto .= "type = {$this->configuracoes['type']}\n";

                    $handle = fopen($this->arquivo, 'w');
                    fwrite($handle, $texto);
                    fclose($handle);
                }

                catch (Exception $e)
                {
                    echo $e->getMessage();
                }
            }
            else
            {
                throw new Exception("Parâmetro de configuração não informado!");
            }
        }
    }

    /**
     * Método getConfig()
     * <pre>
     * Método usado para retornar as informações de um arquivo de configuração
     * </pre>
     * @param string $arquivo Nome do arquivo de configuração
     * @return array
     */
    public function getConfig()
    {
        //Verifica se o arquivo existe
        if(file_exists($this->arquivo))
        {
            $this->configuracoes = parse_ini_file($this->arquivo);

            $configuracoes = $this->configuracoes;

            //Retorna o array de configurações
            return $configuracoes;
        }
        else
        {
            throw new Exception("O arquivo informado não existe");
        }
    }
}

?>
