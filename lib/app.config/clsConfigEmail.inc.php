<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.config
 */
/**
 * Classe clsConfigEmail
 * <pre>
 * Controla as configurações de conexão com o servidor de email.
 * </pre>
 */
class clsConfigEmail
{
    /**
     * @var string Armazena o nome do arquivo que será manipulado
     * @access private
     */
    private $arquivo;

    /**
     * @var array Armazena as configurações do arquivo
     * @access private
     */
    private $configuracoes;

    /**
     * Método construtor
     * <pre>
     * Define o arquivo que será manipulado na hora da instancia do objeto
     * </pre>
     * @param string $arquivo Nome do arquivo que será manipulado pelo objeto
     */
    public function  __construct($arquivo)
    {
        //Seta a localização, o nome e a extenção INI para arquivo de configuração
        $this->arquivo = "/var/www/intravip/trunk/sys/app.config/".$arquivo.".ini";
    }

    /**
     * Método setConfig()
     * <pre>
     * Método usado para modificar as informações contidas no arquivo de configuraçao
     * </pre>
     * @param string $host Endereço do host do servidor de email
     * @param string $port Porta usada na conexão
     * @param string $driver Driver usado para conexão (Aceitos. pop/imap)
     * @param string $ssl Define se vai usar ssl ou não (True or False)
     * @param string $novalidate Define se vai validar o certificado ou não (True or False)
     */
    public function setConfig($host = null, $port = null, $driver = null, $ssl = null, $novalidate = null)
    {
        //Verifica se o arquivo existe
        if(file_exists($this->arquivo))
        {
            //Carrega as configurações do arquivo
            $this->configuracoes = parse_ini_file($this->arquivo);

            if($host)
            {
                $this->configuracoes['host'] = $host;
            }
            if($port)
            {
                $this->configuracoes['port'] = $port;
            }
            if($driver)
            {
                $this->configuracoes['driver'] = $driver;
            }
            if($ssl)
            {
                $this->configuracoes['ssl'] = $ssl;
            }
            if($novalidate)
            {
                $this->configuracoes['novalidate'] = $novalidate;
            }

            try
            {
                $texto  = "host = {$this->configuracoes['host']}\n";
                $texto .= "port = {$this->configuracoes['port']}\n";
                $texto .= "driver = {$this->configuracoes['name']}\n";
                $texto .= "ssl = {$this->configuracoes['user']}\n";
                $texto .= "novalidate = {$this->configuracoes['pass']}\n";

                $handle = fopen($this->arquivo, 'w');
                fwrite($handle, $texto);
                fclose($handle);
            }

            catch (Exception $e)
            {
                echo $e->getMessage();
            }

        }
        //Se não existir, cria o arquivo com as configurações passadas
        else
        {
            if($host && $port && $driver && $ssl && $novalidate)
            {
                $this->configuracoes['host'] = $host;
                $this->configuracoes['port'] = $port;
                $this->configuracoes['driver'] = $driver;
                $this->configuracoes['ssl'] = $ssl;
                $this->configuracoes['novalidate'] = $novalidate;

                try
                {
                    $texto  = "host = {$this->configuracoes['host']}\n";
                    $texto .= "port = {$this->configuracoes['port']}\n";
                    $texto .= "driver = {$this->configuracoes['driver']}\n";
                    $texto .= "ssl = {$this->configuracoes['ssl']}\n";
                    $texto .= "novalidate = {$this->configuracoes['novalidate']}\n";

                    $handle = fopen($this->arquivo, 'w');
                    fwrite($handle, $texto);
                    fclose($handle);
                }

                catch (Exception $e)
                {
                    echo $e->getMessage();
                }
            }
            else
            {
                throw new Exception("Parâmetro de configuração não informado!");
            }
        }
    }

    /**
     * Método getConfig()
     * <pre>
     * Método usado para retornar as informações de um arquivo de configuração
     * </pre>
     * @return array
     */
    public function getConfig()
    {
        //Verifica se o arquivo existe
        if(file_exists($this->arquivo))
        {
            $this->configuracoes = parse_ini_file($this->arquivo);

            $configuracoes = $this->configuracoes;

            //Retorna o array de configurações
            return $configuracoes;
        }
        else
        {
            throw new Exception("O arquivo informado não existe");
        }
    }
}

?>
