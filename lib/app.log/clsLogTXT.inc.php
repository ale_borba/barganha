<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.log
 */
/**
 * Classe clsLogHTML filha de clsLog
 * <pre>
 * Aplica o algoritimo de LOG em formato HTML
 * </pre>
 */
class clsLogTXT extends clsLog
{
    /**
     * Método escrever()
     * <pre>
     * Escreve uma mensagem no arquivo de LOG
     * </pre>
     * @param string $mensagem Mensagem a ser escrita no arquivo de LOG
     */
    public function escrever($mensagem)
    {
        //Armazena o horário do LOG
        $time = date('Y-m-d H:i:s');

        //Define o usuário
        new clsSessao;
        $usuario = clsSessao::getValor('usuario');

        $texto = "{$_SERVER['REMOTE_ADDR']} :: {$usuario} :: {$time} :: {$mensagem}\n";

        //Grava as informações no final do arquivo
        $handle = fopen($this->arquivo.'.txt', 'a');
        fwrite($handle, $texto);
        fclose($handle);
    }
}

?>
