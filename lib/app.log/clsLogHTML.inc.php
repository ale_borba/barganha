<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.log
 */
/**
 * Classe clsLogHTML filha de clsLog
 * <pre>
 * Aplica o algoritimo de LOG em formato HTML
 * </pre>
 */
class clsLogHTML extends clsLog
{
    /**
     * Método escrever()
     * <pre>
     * Escreve uma mensagem no arquivo de LOG
     * </pre>
     * @param string $mensagem Mensagem a ser escrita no arquivo de LOG
     */
    public function escrever($mensagem)
    {
        //Armazena o horário do LOG
        $time = date('Y-m-d H:i:s');

        //Define o usuário
        new clsSessao;
        $usuario = clsSessao::getValor('usuario');

        //Verifica se o arquivo de LOG já existe. Se não, cria e acrescenta o LOG. Se sim, acrescenta o LOG ao final.
        if(!file_exists($this->arquivo.'.html'))
        {
            //Monta a string a ser armazenada no arquivo de LOG
            $texto  = "<html>\n";
            $texto .= " <title>Conte&uacute;do do arquivo de LOG em HTML</title>\n";
            $texto .= " <body>\n";
            $texto .= "     <p><b>{$_SERVER['REMOTE_ADDR']}</b>\n";
            $texto .= "     <p><i>{$usuario}</i>\n";
            $texto .= "     <b>{$time}</b>\n";
            $texto .= "     <i>{$mensagem}</i></p>\n";
            $texto .= " </body>\n";
            $texto .= "</html>\n";

            //Grava as informações no arquio de LOG
            $handle = fopen($this->arquivo.'.html', 'a');
            fwrite($handle, $texto);
            fclose($handle);
        }
        else
        {
            //Monta a string a ser armazenada no arquivo de LOG
            $texto  = "<html>\n";
            $texto .= " <title>Conte&uacute;do do arquivo de LOG em HTML</title>\n";
            $texto .= " <body>\n";
            $texto .= "     <p><b>{$_SERVER['REMOTE_ADDR']}</b>\n";
            $texto .= "     <p><i>{$usuario}</i>\n";
            $texto .= "     <p><b>{$time}</b>\n";
            $texto .= "     <i>{$mensagem}</i></p>\n";

            //Pega as informações existentes e reserva para acrescentar no fim do arquivo
            $infoLog = file_get_contents($this->arquivo.'.html',false,null, 72);

            //Acrescenta ao final do arquivo
            $texto .= $infoLog;

            //Grava as informações no arquivo de LOG colocando as informações antigas abaixo do novo log
            $handle = fopen($this->arquivo.'.html', 'w');
            fseek($handle, -6);
            fwrite($handle, $texto);
            fclose($handle);
        }
    }
}

?>
