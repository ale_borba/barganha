<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.log
 */
/**
 * Classe clsLogXML filha de clsLog
 * <pre>
 * Aplica o algoritmo de LOG em formato XML
 * </pre>
 */
class clsLogXML extends clsLog
{
    /**
     * Método escrever()
     * <pre>
     * Escreve uma mensagem no arquivo de LOG
     * </pre>
     * @param string $mensagem Mensagem a ser escrita no arquivo de LOG
     */
    public function escrever($mensagem)
    {
        //Armazena o horário do LOG
        $time = date('Y-m-d H:i:s');

        //Define o usuário
        new clsSessao;
        $usuario = clsSessao::getValor('usuario');

        //Verifica se o arquivo de LOG já existe. Se não, cria e acrescenta o LOG. Se sim, acrescenta o LOG ao final.
        if(!file_exists($this->arquivo.'.xml'))
        {
            //Monta a string a ser armazenada no arquivo de LOG
            $texto  = "<log>\n";
            $texto .= " <ip>{$_SERVER['REMOTE_ADDR']}</ip>\n";
            $texto .= " <user>{$usuario}</user>\n";
            $texto .= " <time>{$time}</time>\n";
            $texto .= " <mensagem>{$mensagem}</mensagem>\n";
            $texto .= "</log>\n";

            //Grava as informações no arquio de LOG
            $handle = fopen($this->arquivo.'.xml', 'a');
            fwrite($handle, $texto);
            fclose($handle);
        }
        else
        {
            //Monta a string a ser armazenada no arquivo de LOG
            $texto  = "<log>\n";
            $texto .= " <ip>{$_SERVER['REMOTE_ADDR']}</ip>\n";
            $texto .= " <user>{$usuario}</user>\n";
            $texto .= " <time>{$time}</time>\n";
            $texto .= " <mensagem>{$mensagem}</mensagem>\n";

            //Pega as informações existentes e reserva para acrescentar no fim do arquivo
            $infoLog = file_get_contents($this->arquivo.'.xml',false,null, 5);

            //Acrescenta ao final do arquivo
            $texto .= $infoLog;

            //Grava as informações no arquivo de LOG colocando as informações antigas abaixo do novo log
            $handle = fopen($this->arquivo.'.xml', 'w');
            fseek($handle, -6);
            fwrite($handle, $texto);
            fclose($handle);
        }
    }
}

?>
