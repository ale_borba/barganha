<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.log
 */
/**
 * Classe clsLog
 * <pre>
 * Classe abstrata que define uma interface para algoritmos de LOG
 * </pre>
 * @abstract
 */
abstract class clsLog
{
    /**
     * Varirável que armazena o local do arquivo de LOG
     * @var string
     * @access protected
     */
    protected $arquivo;

    /**
     * Método construtor
     * <pre>
     * Cria um novo LOG
     * </pre>
     * @param string $arquivo Localização do arquivo de LOG
     */
    public function  __construct($arquivo)
    {
        //Verifica se existem os diretórios que irão armazenar os arquivos de LOG. Se não eles são criados.
        if(!is_dir('tmp'))
        {
            if(mkdir('tmp'))
            {
                if(mkdir('tmp/log'))
                {
                    $this->arquivo = 'tmp/log/'.$arquivo;
                }
                else
                {
                    throw new Exception("Não foi possível criar o diretório tmp/log/");
                }
            }
            else
            {
                throw new Exception("Não foi possível criar o diretório tmp/");
            }
        }
        else if(!is_dir('tmp/log'))
        {
            if(mkdir('tmp/log'))
            {
                $this->arquivo = 'tmp/log/'.$arquivo;
            }
            else
            {
                throw new Exception("Não foi possível criar o diretório tmp/log/");
            }
        }
        else
        {
            $this->arquivo = 'tmp/log/'.$arquivo;
        }
    }

    /**
     * Método escrever()
     * <pre>
     * Marcado como abstrato para se tornar obrigatório em todas as classes filhas
     * </pre>
     * @param string $mensagem Mensagem a ser gravada no arquivo de LOG
     * @abstract
     */
    abstract function escrever($mensagem);
}

?>
