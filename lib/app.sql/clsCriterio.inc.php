<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.sql
 */
/**
 * Classe clsCriterio filha de clsExpressao
 * <pre>
 * Uma interface para definição de critérios de seleção de dados
 */
class clsCriterio extends clsExpressao
{
    /**
     * @access private
     * @var array
     */
    private $expressoes;

    /**
     * @access private
     * @var array
     */
    private $operadores;

    /**
     * @access private
     * @var array
     */
    private $propriedades;

    /**
     * Método adicionar()
     * <pre>
     * Adiciona uma expressão ao critério de seleção
     * </pre>
     * @param clsExpressao $expressao Objeto do tipo clsExpressao
     * @param const $operador Operador lógico de comparação
     * @access public
     */
    public function adicionar(clsExpressao $expressao, $operador = self::OPERADOR_AND)
    {
        //Verifica se é a primeira entrada. Se positivo não seta o operador
        if(empty ($this->expressoes))
        {
            unset ($operador);
        }

        //Agrega os valores às listas de expressões
        $this->expressoes[] = $expressao;
        @$this->operadores[] = $operador;
    }

    /**
     * Método dump()
     * <pre>
     * Retorna o resultado em forma de expressão
     * </pre>
     * @access public
     * @return string
     */
    public function dump()
    {
        //Monta toda a lista de expressões
        if(is_array($this->expressoes))
        {
            foreach ($this->expressoes as $idx => $expressao)
            {
                $operador = $this->operadores[$idx];
                @$resultado .= $operador.$expressao->dump().' ';
            }

            //Retira os espaços vazios do inicio e do final da expressão
            $resultado = trim($resultado);

            return "({$resultado})";
        }
    }

    /**
     * Método setPropriedade()
     * <pre>
     * Define o valor de uma propriedade, como por exemplo, order by e limit.
     * </pre>
     * @param string $propriedade Nome da propriedade. Ex. ORDER BY, LIMIT, OFFSET
     * @param mixed $valor Valor da propriedade.
     * @access public
     */
    public function setPropriedade($propriedade, $valor)
    {
        $this->propriedades[$propriedade] = $valor;
    }

    /**
     * Método getPropriedade()
     * <pre>
     * Retorna o valor de uma propriedade
     * </pre>
     * @param string $propriedade Nome da propriedade a ser retornada
     * @return mixed
     * @access public
     */
    public function getPropriedade($propriedade)
    {
        return @$this->propriedades[$propriedade];
    }
}

?>