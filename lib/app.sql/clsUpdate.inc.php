<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.sql
 */
/**
 * Classe clsUpdate filha de clsComandos
 * <pre>
 * Classe que manipula comandos de INSERT no banco de dados
 * </pre>
 */
final class clsUpdate extends clsComandos
{
    /**
     * Método setColunaValor()
     * <pre>
     * Atribui valores a serem modificados nas determinadas colunas do banco de dados
     * </pre>
     * @access public
     * @param string $coluna Nome da coluna que terá o valor modificado
     * @param string $valor Valor a ser inserido
     */
    public function setColunaValor($coluna, $valor)
    {
        //Monta um array usando como index o nome da coluna
        if(is_string($valor))
        {
            //Adiciona \ onde aparecer aspas
            $valor = addslashes($valor);

            $this->colunaValor[$coluna] = "'$valor'";
        }
        else if(is_bool($valor))
        {
            $this->colunaValor[$coluna] = $valor ? 'TRUE' : 'FALSE';
        }
        else if(isset ($valor))
        {
            $this->colunaValor[$coluna] = $valor;
        }
        else
        {
            $this->colunaValor[$coluna] = 'NULL';
        }
    }

    /**
     * Método getInstrucao()
     * <pre>
     * Retorna o comando de UPDATE em forma de string
     * </pre>
     * @access public
     * @return string
     */
    public function getInstrucao()
    {
        $this->sql = "UPDATE {$this->tabela}";

        //Monta os pares de coluna e valor
        if($this->colunaValor)
        {
            foreach ($this->colunaValor as $coluna => $valor)
            {
                $set[] = "{$coluna} = {$valor}";
            }
        }

        //Concatena a informação
        $this->sql .= " SET ".implode(',', $set);

        //Concatena o critério de seleção na cláusula WHERE
        if($this->criterio)
        {
            $this->sql .= " WHERE ".$this->criterio->dump();
        }

        return $this->sql;
    }
}

?>
