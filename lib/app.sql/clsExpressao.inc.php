<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.sql
 */
/**
 * Classe clsExpressao
 * <pre>
 * Classe abstrata para tratamento de expressões OR e AND
 * </pre>
 * @abstract
 */
abstract class clsExpressao
{
    //Define as constantes para os operadores lógicos OR e AND
    const OPERADOR_AND  = 'AND ';
    const OPERADOR_OR   = 'OR ';

    /**
     * Método dump()
     * <pre>
     * Obrigatório a todas às Classes filhas
     * </pre>
     * @abstract
     */
    abstract function dump();
}
?>
