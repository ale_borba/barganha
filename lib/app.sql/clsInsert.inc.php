<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.sql
 */
/**
 * Classe clsInsert filha de clsComandos
 * <pre>
 * Classe que manipula comandos de INSERT no banco de dados
 * </pre>
 */
final class clsInsert extends clsComandos
{
    /**
     * Método setColunaValor()
     * <pre>
     * Atribui valores a serem inseridos nas determinadas colunas do banco de dados
     * </pre>
     * @access public
     * @param string $coluna Nome da coluna que recebera o valor
     * @param string $valor Valor a ser armazenado
     */
    public function setColunaValor($coluna, $valor)
    {
        //Monta um array usando como index o nome da coluna
        if(is_string($valor))
        {
            //Adiciona \ onde aparecer aspas
            $valor = addslashes($valor);

            $this->colunaValor[$coluna] = "'$valor'";
        }
        else if(is_bool($valor))
        {
            $this->colunaValor[$coluna] = $valor ? 'TRUE' : 'FALSE';
        }
        else if(isset ($valor))
        {
            $this->colunaValor[$coluna] = $valor;
        }
        else
        {
            $this->colunaValor[$coluna] = 'NULL';
        }
    }

    /**
     * Método setCriterio()
     * <pre>
     * Nao existe critério de seleção em uma instrução de INSERT.
     * Dessa forma, se for executado lançará um erro.
     * </pre>
     * @access public
     */
    public function setCriterio($criterio)
    {
        throw new Exception("O método setCriterio() não pode ser chamado por".__CLASS__);
    }

    /**
     * Método getInstrucao()
     * <pre>
     * Retorna o comando SQL em forma de string
     * </pre>
     * @access public
     * @return string
     */
    public function getInstrucao()
    {
        //Monta uma string contendo o nome das colunas
        $colunas = implode(',', array_keys($this->colunaValor));
        //Monta uma string contendo os valores
        $valores = implode(',', array_values($this->colunaValor));

        //Monta o comando SQL
        $this->sql = "INSERT INTO {$this->tabela} ({$colunas}) VALUES ({$valores})";

        return $this->sql;
    }
}

?>
