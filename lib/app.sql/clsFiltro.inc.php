<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.sql
 */
/**
 * Classe clsFiltro filha de clsExpressao
 * <pre>
 * Interface para definição dos filtros de seleção no banco de dados
 */
class clsFiltro extends clsExpressao
{
    /**
     * @access private
     * @var mixed
     */
    private $variavel;

    /**
     * @access private
     * @var mixed
     */
    private $operador;

    /**
     * @access private
     * @var mixed
     */
    private $valor;

    /**
     * Método construtor
     * <pre>
     * Instancia um novo filtro
     * </pre>
     * @access public
     * @param string $variavel Variavel que sera passada como critério para o filtro.
     * @param string $operador Operador que será usado na comparação (=,>,<,etc).
     * @param mixed $valor Valor ao qual a variável será comparada.
     */
    public function  __construct($variavel, $operador, $valor)
    {
        //Faz as atribuições necessárias das propriedades
        $this->variavel = $variavel;
        $this->operador = $operador;

        /* Faz as devidas verificações no parametro $valor através do
         * método transforma() antes de atribuir o à propriedade
         * $this->valor
         */
        $this->valor    = $this->transforma($valor);
    }

    /**
     * Método transforma()
     * <pre>
     * Modifica um valor para que ele seja corretamente interpretado pelo banco de dados
     * </pre>
     * @access private
     * @param int|string|boolean|array $valor Valor a ser transformado
     * @return string
     */
    private function transforma($valor)
    {
        //Verifica se é um array
        if(is_array($valor))
        {
            //Faz a varredura em todos os valores
            foreach ($valor as $v)
            {
                //Se for string ele inclui aspas para o banco aceitar como string
                if (is_string($v))
                {
                    //Verifica se existe algum comando interno do banco.
                    $parenteses = strpos($v, '(');

                    if ($parenteses)
                    {
                        $comando = explode('(', $v);
                        //Retira o parenteses final.
                        $comando[1] = str_replace(')', '', $comando[1]);

                        //Monta o resultado com o comando.
                        $resultados[] = $comando[0]."('$comando[1]')";
                    }
                    else
                    {
                        $resultados[] = "'$v'";
                    }
                }

                //Se for inteiro ele apenas atribui o valor
                if(is_integer($v))
                {
                    $resultados[] = $v;
                }

                //Pega o array $resultados[] e converte em uma string separada por vírgulas
                $resultado = '('.implode(',', $resultados).')';
            }
        }
        //Verifica se é uma string. Se positivo adiciona aspas.
        else if(is_string($valor))
        {
            //Verifica se existe algum comando interno do banco.
            $parenteses = strpos($valor, '(');

            if ($parenteses)
            {
                $comando = explode('(', $valor);
                //Retira o parenteses final.
                $comando[1] = str_replace(')', '', $comando[1]);

                //Monta o resultado com o comando.
                $resultado = $comando[0]."('$comando[1]')";
            }
            else
            {
                $resultado = "'$valor'";
            }
        }
        //Verifica se é NULL. Se positivo armazena NULL
        else if(is_null($valor))
        {
            $resultado = "NULL";
        }
        //Verifica se é um valor boleano.
        else if(is_bool($valor))
        {
            $resultado = $valor ? 'TRUE' : 'FALSE';
        }
        else
        {
            $resultado = $valor;
        }

        return $resultado;
    }

    /**
     * Método dump()
     * <pre>
     * Monta todo o filtro e retorna em forma de expressão
     * </pre>
     * @access public
     * @return string
     */
    public function dump()
    {
        return "{$this->variavel} {$this->operador} {$this->valor}";
    }
}

?>
