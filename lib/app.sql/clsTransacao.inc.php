<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.sql
 */
/**
 * Classe clsTransacao
 * <pre>
 * Classe que manipula todas as transações com o banco de dados
 * </pre>
 */
final class clsTransacao
{
    /**
     * @staticvar armazena o Objeto PDO da conexão
     * @access private
     */
    private static $con;

    /**
     * @staticvar armazena o Objeto de LOG
     * @access private
     */
    private static $log;

    /**
     * Método Construtor
     * <pre>
     * Declarado como private para evitar que se instanciem objetos do tipo clsTransacao
     * </pre>
     */
    private function  __construct(){}

    /**
     * Método open()
     * <pre>
     * Abre uma conexão com o banco de dados e começa uma transação
     * </pre>
     * @param string $banco Nome do banco de dados a ser utilizado na transação
     * @static
     */
    public static function open($banco)
    {
        if(empty (self::$con))
        {
            self::$con = clsConexao::open($banco);
            self::$con->beginTransaction();

            //Limpa o objeto LOG
            self::$log = null;
        }
    }

    /**
     * Método setLog()
     * <pre>
     * Define qual o algoritimo de LOG será usado
     * </pre>
     * @param clsLog $log Objeto de LOG que define qual a estratégia a ser usada
     * @static
     */
    public static function setLog(clsLog $log)
    {
        self::$log = $log;
    }

    /**
     * Método log()
     * <pre>
     * Armazena uma mensagem de log no arquivo baseado no algoritimo de LOG escolhido
     * </pre>
     * @param $string $mensagem Mensagem a ser escrita no arquivo
     * @static
     */
    public static function log($mensagem)
    {
        if(self::$log)
        {
            self::$log->escrever($mensagem);
        }
    }

    /**
     * Método get()
     * <pre>
     * Retorna a conexão ativa da transação
     * </pre>
     * @return Objeto PDO ativo
     * @static
     */
    public static function get()
    {
        return self::$con;
    }

    /**
     * Método rollback()
     * <pre>
     * Desfaz todas as operações realizadas durante a transação e fecha a transação
     * </pre>
     * @static
     */
    public static function rollback()
    {
        if(self::$con)
        {
            self::$con->rollback();
            self::$con = null;
        }
    }

    /**
     * Método close()
     * <pre>
     * Confirma todas as operações realizadas durante a transação e fecha a transação
     * </pre>
     * @static
     */
    public static function close()
    {
        if(self::$con)
        {
            self::$con->commit();
            self::$con = null;
        }
    }
}

?>
