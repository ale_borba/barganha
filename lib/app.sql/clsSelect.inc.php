<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.sql
 */
/**
 * Classe clsSelect filha de clsComandos
 * <pre>
 * Classe que manipula comandos de SELECT no banco de dados
 * </pre>
 */
final class clsSelect extends clsComandos
{
    /**
     * Variável que guarda um array de colunas a serem retornadas no comando SELECT
     * @var array
     * @access private
     */
    private $colunas;

    /**
     * Método addColuna()
     * <pre>
     * Adiciona uma coluna ao array de colunas a serem retornadas no comando SELECT
     * </pre>
     * @param string $coluna Nome da coluna a ser retornada no SELECT
     * @access public
     */
    public function addColuna($coluna)
    {
        //Adiciona a coluna ao array de colunas
        $this->colunas[] = $coluna;
    }

    /**
     * Método getInstrucao()
     * <pre>
     * Retorna o comando SELECT em forma de string
     * </pre>
     * @return string
     * @access public
     */
    public function getInstrucao()
    {
        //Monta o comando de SELECT com o nome das colunas e adiciona o nome da tabela ao FROM
        $this->sql  = 'SELECT ';
        $this->sql .= implode(',', $this->colunas);
        $this->sql .= ' FROM '.$this->tabela;

        //Pega o valor do critério para a cláusula WHERE
        if($this->criterio)
        {
            $expressao = $this->criterio->dump();

            if($expressao)
            {
                $this->sql .= ' WHERE '.$expressao;
            }

            //Verifica as propriedades do critério e se existirem adiciona ao final
            $order  = $this->criterio->getPropriedade('order');
            $limit  = $this->criterio->getPropriedade('limit');
            $offset = $this->criterio->getPropriedade('offset');

            if($order)
            {
                $this->sql .= ' ORDER BY '.$order;
            }

            if($limit)
            {
                $this->sql .= ' LIMIT '.$limit;
            }

            if($offset)
            {
                $this->sql .= ' OFFSET '.$offset;
            }
        }

        return $this->sql;
    }

     /**
     * Método getCurval()
     * <pre>
     * Retorna o comando SELECT curval em forma de string
     * </pre>
     * @return string
     * @access public
     */
    public function getCurval($tabela = null)
    {
        //Se não for passado valor de $tabela ele incorpora a tabela ativa
        if (!$tabela)
        {
            $tabela = $this->tabela;
        }

        //Monta o nome da tabela de sequencia
        $tabela_id = str_replace('tbl', 'id', $tabela);
        $tabela_seq = $tabela."_".$tabela_id."_seq";

        //Monta o comando de SELECT currval com o nome da tabela
        $this->sql  = 'SELECT ';
        $this->sql .= "currval('{$tabela_seq}')";

        return $this->sql;
    }
}

?>
