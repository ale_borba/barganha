<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.sql
 */
/**
 * Classe clsConexao
 * <pre>
 * Gerencia todas as conexões ao banco de dados através de arquivos de configuração
 * </pre>
 */
final class clsConexao
{
    /**
     * Método construtor
     * <pre>
     * Marcado como private pois não poderão existir instancias dessa classe.
     * </pre>
     */
    private function  __construct(){}

    /**
     * Método open()
     * <pre>
     * Recebe o nome do banco de dados e instancia o PDO correspondente
     * </pre>
     * @param string $nome Nome do banco de dados a ser utilizado
     * @return Objeto PDO
     * @static
     */
    public static function open($nome)
    {
        $sys = strpos($_SERVER['REQUEST_URI'], 'sys');
        if ($sys)
        {
            //Confere se existe arquivo de configuraçao criado para esse banco de dados. Se não existir lança um erro
            if(file_exists("app.config/{$nome}.ini"))
            {
                //Retorna um array com as informações do arquivo .ini
                $infoBanco = parse_ini_file("app.config/{$nome}.ini");
            }
            else
            {
                throw new Exception("Não existe arquivo de configuração para {$nome}");
            }
        }
        else
        {
            //Confere se existe arquivo de configuraçao criado para esse banco de dados. Se não existir lança um erro
            if(file_exists("sys/app.config/{$nome}.ini"))
            {
                //Retorna um array com as informações do arquivo .ini
                $infoBanco = parse_ini_file("sys/app.config/{$nome}.ini");
            }
            else
            {
                throw new Exception("Não existe arquivo de configuração para {$nome}");
            }
        }

        //Armazena as configurações do arquivo
        $dbuser = $infoBanco['user'];
        $dbpass = $infoBanco['pass'];
        $dbname = $infoBanco['name'];
        $dbhost = $infoBanco['host'];
        $dbtype = $infoBanco['type'];
        $dbport = $infoBanco['port'];

        //Verifica qual tipo de banco de dados será utilizado e instancia o PDO correspondente
        switch ($dbtype)
        {
            case 'pgsql':
                $con = new PDO("pgsql:dbname={$dbname};user={$dbuser};password={$dbpass};host={$dbhost};port={$dbport}");
                break;
            case 'mysql':
                $con = new PDO("mysql:host={$dbhost};port={$dbport};dbname={$dbname}, {$dbuser}, {$dbpass}");
                break;
        }

        //Seta os atributos de erros para o PDO
        $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $con;
    }
}

?>
