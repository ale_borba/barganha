<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.sql
 */
/**
 * Classe clsComandos
 * <pre>
 * Classe abstrata que define todos os métodos comuns entre os comandos de SQL (INSERT, UPDATE, SELECT, DELETE)
 * @abstract
 */
abstract class clsComandos
{
    /**
     * @access protected
     * @var string
     */
    protected $sql;

    /**
     * @access protected
     * @var clsCriterio
     */
    protected $criterio;

    /**
     * Método setTabela()
     * <pre>
     * Define o nome da Tabela que será manipulada pelo comando SQL
     * </pre>
     * @access public
     * @param string $tabela Nome da tabela que será manipulada
     */
    final public function setTabela($tabela)
    {
        $this->tabela = $tabela;
    }

    /**
     * Método getTabela()
     * <pre>
     * Retorna o nome da tabela que está sendo manipulada
     * </pre>
     * @access public
     * @return string
     */
    final public function getTabela()
    {
        return $this->tabela;
    }

    /**
     * Método setCriterio()
     * <pre>
     * Define um critério para seleção de dados baseado na composição do objeto clsCriterio
     * </pre>
     * @access public
     * @param clsCriterio $criterio Composição de um objeto tipo clsCriterio
     */
    public function setCriterio(clsCriterio $criterio)
    {
        $this->criterio = $criterio;
    }

    /**
     * Método getInstrucao()
     * <pre>
     * Método abstrato de declaração obrigatória em todas as classes filhas.
     * </pre>
     * @abstract
     */
    abstract function getInstrucao();
}

?>
