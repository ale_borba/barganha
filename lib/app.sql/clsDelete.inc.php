<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.sql
 */
/**
 * Classe clsDelete filha de clsComandos
 * <pre>
 * Classe que manipula comandos de DELETE no banco de dados
 * </pre>
 */
final class clsDelete extends clsComandos
{
    /**
     * Método getInstrucao()
     * <pre>
     * Retorna o comando DELETE em forma de string
     * </pre>
     * @return string
     * @access public
     */
    public function getInstrucao()
    {
        //Monta o comando de DELETE e adiciona o nome da tabela ao FROM
        $this->sql = 'DELETE FROM '.$this->tabela;

        //Pega o valor do critério para a cláusula WHERE
        if($this->criterio)
        {
            $expressao = $this->criterio->dump();

            if($expressao)
            {
                $this->sql .= ' WHERE '.$expressao;
            }
        }

        return $this->sql;
    }
}
?>
