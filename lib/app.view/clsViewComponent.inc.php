<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.view
 */
/**
 * Classe abstrata que declara a interface para todos os objetos da composição
 * 
 * @abstract
 */
abstract class clsViewComponent {
    /**
     * Lista de filhos
     *
     * @var ArrayObject
     * @access protected
     */
    protected $children;

    /**
     * Atributo ID do elemento
     *
     * @var string
     * @access private
     */
    private $id;

    /**
     * Define se o componente é um Leaf ou um Composite
     *
     * @var boolean
     * @access protected
     */
    protected $leaf = false;

    /**
     * Method __construct()
     *
     * Constroi o componente
     *
     * @access public
     */
    public function  __construct() {
        if (!$this->leaf) {
            $this->children = new ArrayObject();
        }

        $this->id = uniqid('id');
    }

    /**
     * Method add()
     *
     * Adiciona um filho a estrutura
     *
     * @param ViewComponent $component The component object
     * @access public
     */
    public function add(clsViewComponent $component) {
        if (!$this->leaf) {
            $this->children->append($component);
        }
        else {
            throw new LogicException("A Leaf don't have children");
        }
    }

    /**
     * Method drawChildren()
     *
     * Desenha todos os filhos da composição
     *
     * @access protected
     */
    protected function drawChildren() {
        if (!$this->leaf) {
            $iterator = $this->children->getIterator();

            for ($iterator->rewind();$iterator->valid();$iterator->next()){
                $iterator->current()->draw();
            }
        }
    }

    /**
     * Method getID()
     *
     * Recupera o ID do elemento
     *
     * @return string
     * @access public
     */
    public function getID() {
        return $this->id;
    }

    /**
     * Method draw()
     *
     * Método abstrato para desenhar o componente que deve ser aplicado por
     * todas as classes filhas
     *
     * @abstract
     */
    abstract function draw();
}
?>
