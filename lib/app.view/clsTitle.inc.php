<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.view
 */
/**
 * Classe Title filha de ViewComponet
 *
 * Implementa um component leaf TITLE
 */
class clsTitle extends clsViewComponent{
    /**
     * Conteúdo do TITLE
     *
     * @var string
     * @access private
     */
    private $content;

    /**
     * Method __construct()
     *
     * @param string $content The title contents
     * @access public
     */
    public function __construct($content){
        parent::__construct();

        $this->content = $content;
    }

    /**
     * Method draw()
     *
     * Desenha o componete leaf TITLE
     *
     * @access public
     */
    public function draw() {
        echo "<title>{$this->content}</title>\n";
    }
}
?>