<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.view
 */
/**
 * Classe Body filha de ViewComponent
 *
 * Implementa um componente do tipo body
 *
 */
class clsBody extends clsViewComponent {
    /**
     * Method __construct()
     *
     * Constroi o HTML
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Method draw()
     *
     * Desenha o HTML
     */
    public function draw() {
        echo "<body>";
        $this->drawChildren();
        echo "</body>\n";
    }
}
?>