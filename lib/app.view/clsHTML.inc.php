<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.view
 */
/**
 * Classe HTML filha de ViewComponent
 *
 * Implementa um componente do tipo HTML
 * 
 */
class clsHTML extends clsViewComponent {
    /**
     * Method __construct()
     *
     * Constroi o HTML
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Method draw()
     *
     * Desenha o HTML
     */
    public function draw() {
        echo "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>\n";
        echo "<html>";
        $this->drawChildren();
        echo "</html>\n";
    }
}
?>