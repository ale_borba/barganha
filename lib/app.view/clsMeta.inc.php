<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.view
 */
/**
 * Classe Meta filha de ViewComponent
 *
 * Implementa um component Leaf do tipo META
 */
class clsMeta extends clsViewComponent{
    /**
     * Define o componente como um Leaf
     *
     * @var boolean
     * @access protected
     */
    protected $leaf = true;

    /**
     * Atributo content da tag meta
     *
     * @var string
     * @access private
     */
    private $content;

    /**
     * Atributo charset do projeto
     *
     * @var string
     * @access private
     */
    private $charset;

    /**
     * Method __construct()
     *
     * Constroi o componente META
     *
     * @param string $content Content-type value
     * @param string $charset Charset value
     *
     * @access public
     */
    public function __construct($content = 'text/html' , $charset = 'utf-8'){
        parent::__construct();

        $this->content  = $content;
        $this->charset  = $charset;
    }

    /**
     * Method draw()
     *
     * Desenha o componente META
     *
     * @access public
     */
    public function draw() {
        echo "<meta http-equiv='Content-Type' content='{$this->content}; charset={$this->charset}'>\n";
    }
}
?>