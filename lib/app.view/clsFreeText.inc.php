<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.view
 */
/**
 * Classe FreeText filha de ViewComponent
 *
 * Implementa um texto livre do tipo Leaf. e.g."<b>Exemplo de texto livre</b>"
 */
class clsFreeText extends clsViewComponent{
    /**
     * Define o componente como leaf
     *
     * @var boolean
     * @access protected
     */
    protected $leaf = true;

    /**
     * O conteudo do componente
     *
     * @var string
     * @access private
     */
    private $content;

    /**
     * Method __construct()
     *
     * Constroi o componente FreeText
     *
     * @param string $content The free text to be insert
     * @access public
     */
    public function __construct($content){
        parent::__construct();

        $this->content = $content;
    }

    /**
     * Method draw()
     *
     * Desenha o componente FreeText
     *
     * @access public
     */
    public function draw() {
        echo $this->content;
    }
}
?>