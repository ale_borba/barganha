<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.view
 */
/**
 * Classe DIV filha e ViewComponent
 *
 * Implementa o componente DIV
 */
class clsDiv extends clsViewComponent{
    /**
     * Atributo class da DIV
     *
     * @var string
     * @access private
     */
    private $class;

    /**
     * Method __construct()
     *
     * Constroi o componente DIV
     *
     * @param string $class Value of the class attribute
     * @access public
     */
    public function __construct($class){
        parent::__construct();

        $this->class = $class;
    }

    /**
     * Method draw()
     *
     * Desenha o componente DIV
     */
    public function draw() {
        echo "<div class={$this->class}>";
        $this->drawChildren();
        echo "</div>\n";
    }
}
?>