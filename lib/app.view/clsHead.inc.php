<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.view
 */
/**
 * Classe Head filha de ViewComponent
 *
 * Implementa um componente do tipo head
 */
class clsHead extends clsViewComponent{
    /**
     * Method __construct()
     *
     * Constroi o HEAD
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * Method draw()
     *
     * Desenha o HEAD
     */
    public function draw() {
        echo "<head>";
        $this->drawChildren();
        echo "</head>\n";
    }
}
?>
