<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.gadgets
 */
/**
 * Classe clsRepositorio
 * <pre>
 * Esta classe contém todos os métodos necessários para manipular coleções de objetos.
 * </pre>
 * @final
 */
final class clsRepositorio
{
    /**
     * @var $classe string Nome da classe a ser manipulada pelo repositório
     * @access private
     */
    private $classe;

    /**
     * Método construtor
     * <pre>
     * Cria um repositório de objetos
     * </pre>
     * @param $classe string Classe de objetos a ser manipulada.
     */
    function __construct($classe)
    {
        $this->classe = $classe;
    }

    /**
     * Método getTabela
     * <pre>
     * Método que retorna o nome da tabela usada
     * </pre>
     */
    function getTabela()
    {
        $tabela = strtolower($this->classe);
        //Substitui o cls por tbl_ e retorna
        return str_replace('cls', 'tbl_', $tabela);
    }

    /**
     * Método carregar
     * <pre>
     * Carrega um conjunto de objetos da base de dados através de um critério de seleção
     * e os carrega na memória
     * </pre>
     * @param $criterio clsCriterio Objeto que determina o critério de seleção
     */
    function carregar(clsCriterio $criterio)
    {
        //Cria uma instrução de Select
        $sql = new clsSelect();
        $sql->addColuna('*');
        //Seta a tabela
        $sql->setTabela($this->getTabela());

        //Atribui o critério passado como parametro
        $sql->setCriterio($criterio);

        // Pega a transação ativa
        if ($conexao = clsTransacao::get())
        {
            //Cria a mensagem de log
            clsTransacao::log($sql->getInstrucao());

            //Executa o SQL
            $resultado = $conexao->Query($sql->getInstrucao());

            //Verifica se obteve resultados
            if ($resultado)
            {
                // Percorre os resultados da consulta e retorna o objeto
                while ($linha = $resultado->fetchObject($this->classe))
                {
                    // Armazena tudo em um array de resultados
                    $resultados[] = $linha;
                }
            }

            // Retorna o array
            return @$resultados;
        }
        else
        {
            // Se não existir transação ativa retorna uma exceção
            throw new Exception('Não existe transação ativa!');
        }
    }

    /**
     * Método carregar
     * <pre>
     * Carrega um conjunto de objetos da base de dados
     * e os carrega na memória
     * </pre>
     */
    function carregar_todos()
    {
        //Cria uma instrução de Select
        $sql = new clsSelect();
        $sql->addColuna('*');
        //Seta a tabela
        $sql->setTabela($this->getTabela());

        // Pega a transação ativa
        if ($conexao = clsTransacao::get())
        {
            //Cria a mensagem de log
            clsTransacao::log($sql->getInstrucao());

            //Executa o SQL
            $resultado = $conexao->Query($sql->getInstrucao());

            //Verifica se obteve resultados
            if ($resultado)
            {
                // Percorre os resultados da consulta e retorna o objeto
                while ($linha = $resultado->fetchObject($this->classe))
                {
                    // Armazena tudo em um array de resultados
                    $resultados[] = $linha;
                }
            }

            // Retorna o array
            return $resultados;
        }
        else
        {
            // Se não existir transação ativa retorna uma exceção
            throw new Exception('Não existe transação ativa!');
        }
    }

    /**
     * Método contador
     * <pre>
     * Retorna a quantidade de objetos que satisfazem o critério de seleção
     * </pre>
     * @param $criterio clsCriterio Objeto criterio de seleção
     */
    function contador(clsCriterio $criterio)
    {
        //Cria uma instrução de Select
        $sql = new clsSelect();
        $sql->addColuna('count(*)');
        //Seta a tabela usada
        $sql->setTabela($this->getTabela());

        //Atribui o critério passado no parametro
        $sql->setCriterio($criterio);

        //Verifica se exite transação ativa
        if ($conexao = clsTransacao::get())
        {
            //Grava o log e executa o SQL
            clsTransacao::log($sql->getInstrucao());
            $resultado = $conexao->Query($sql->getInstrucao());

            // Verifica se obteve resultados
            if($resultado)
            {
                $linha = $resultado->fetch();
            }
            //Retorna o resultado obtido
            return $linha[0];
        }
        else
        {
            // Se não achar transação ativa retorna uma exceção
            throw new Exception('Não existe transação ativa');
        }
    }
}

?>
