<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.gadgets
 */
/**
 * Classe clsEmail
 * <pre>
 * Gerencia as conexões ao servidor de emails através de arquivos de configuração
 * </pre>
 */
final class clsEmail
{
    /**
     * @var Object Armazena a conexão com o servidor de email
     * @access private
     */
    private $con;
    /**
     * Método construtor
     * <pre>
     * Instancia a conexão com o servidor de emails
     * </pre>
     * @return Objeto PDO
     */
    public function  __construct($username = null, $password = null)
    {
        $sys = strpos($_SERVER['REQUEST_URI'], 'sys');
        if ($sys)
        {
            //Confere se existe arquivo de configuraçao criado. Se não existir lança um erro
            if(file_exists("app.config/mail.ini"))
            {
                //Retorna um array com as informações do arquivo .ini
                $infoMail = parse_ini_file("app.config/mail.ini");
            }
            else
            {
                throw new Exception("Não existe arquivo de configuração para o servidor de emails");
            }
        }
        else
        {
            //Confere se existe arquivo de configuraçao criado. Se não existir lança um erro
            if(file_exists("sys/app.config/mail.ini"))
            {
                //Retorna um array com as informações do arquivo .ini
                $infoMail = parse_ini_file("sys/app.config/mail.ini");
            }
            else
            {
                throw new Exception("Não existe arquivo de configuração para o servidor de emails");
            }
        }

        //Armazena as configurações do arquivo
        $mailhost       = $infoMail['host'];
        $mailport       = $infoMail['port'];
        $maildriver     = $infoMail['driver'];
        $mailssl        = $infoMail['ssl'];
        $mailnovalidate = $infoMail['novalidate'];

        //Verifica qual tipo de conexão será utilizada e abre a conexão correspondente
        switch ($maildriver)
        {
            case 'imap':
                if ($mailssl == true) {
                    if ($mailnovalidate ==  true) {
                        $this->con = imap_open("{{$mailhost}:{$mailport}/imap/ssl/novalidate-cert}INBOX", $username, $password);
                    }
                     else
                    {
                        $this->con = imap_open("{{$mailhost}:{$mailport}/imap/ssl}INBOX", $username, $password);
                    }
                }
                 else
                {
                    $this->con = imap_open("{{$mailhost}:{$mailport}/imap}INBOX", $username, $password);
                }
                
                break;
            case 'pop':
                if ($mailssl == true) {
                    if ($mailnovalidate ==  true) {
                        $this->con = imap_open("{{$mailhost}:{$mailport}/pop3/ssl/novalidate-cert}INBOX", $username, $password);
                    }
                     else
                    {
                        $this->con = imap_open("{{$mailhost}:{$mailport}/pop3/ssl}INBOX", $username, $password);
                    }
                }
                 else
                {
                    $this->con = imap_open("{{$mailhost}:{$mailport}/pop3}INBOX", $username, $password);
                }
                break;
        }
    }

    /**
     * Método getRecentes()
     * <pre>
     * Retorna todas as mensagens recentes da caixa de email
     * </pre>
     * @return integer
     */
    public function getRecentes()
    {
        //Faz a consulta no servidor de email e armazena o resultado
        $resultado = imap_check($this->con);

        //Retorna o número de mensagens recentes
        return $resultado->Recent;
    }

    /**
     * Método close()
     * <pre>
     * Fecha a conexão com o servidor de emails
     * </pre>
     */
    public function close()
    {
        //Fecha a conexão
        imap_close($this->con);
    }
}

?>
