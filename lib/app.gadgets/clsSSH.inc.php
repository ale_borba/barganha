<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.gadgets
 */
/**
 * Classe clsSSH
 * <pre>
 * Classe que implementa todas as funções envolvendo comandos e conexões via SSH.
 * </pre>
 */
class clsSSH
{
    /**
     * @var string Armazena o host usado para as conexões
     * @access private
     */
    private $host;
    /**
     * @var integer Armazena a porta usada nas conexões
     * @access private
     */
    private $port;
    /**
     * @var integer Armazena o usuario usado nas conexões
     * @access private
     */
    private $usuario;
    /**
     * @var resource Armazena a conexão SSH
     * @access private
     */
    private $conexao;

    /**
     * Método __construct()
     * <pre>
     * Inicia o novo objeto já criando uma conexão.
     * </pre>
     * @param $chave file Chave de conexão
     */
    public function  __construct()
    {
        //Pega as configurações de host e porta do arquivo de configurações
        $configuracao = new clsConfigBD('ssh');
        $config = $configuracao->getConfig();

        //Seta o host
        $this->host = $config['host'];

        //Seta a porta
        $this->port = $config['port'];

        //Seta o usuario
        $this->usuario = $config['usuario'];

        //Define o método
        $metodo = array('hostkey'=>'ssh-rsa');

        //Abre uma conexão
        $this->conexao = ssh2_connect($this->host, $this->port, $metodo);

        //Faz a autenticação
        $autenticacao = ssh2_auth_pubkey_file($this->conexao, $this->usuario, "/home/{$this->usuario}/.ssh/id_rsa.pub", "/home/{$this->usuario}/.ssh/id_rsa", '');
    }

    /**
     * Método enviarArquivo()
     * <pre>
     * Envia um arquivo para a máquina remota.
     * </pre>
     * @param $caminho_origem string Caminho do arquivo de orgiem
     * @param $arquivo_origem string Nome do arquivo de origem
     * @param $caminho_destino string Caminho do arquivo de destino
     * @param $arquivo_destino string Nome do arquivo de destino
     */
    public function enviarArquivo($caminho_origem, $arquivo_origem, $caminho_destino, $arquivo_destino)
    {
        //Abre uma sessão de SFTP
        $sftp = ssh2_sftp($this->conexao);

        $diretorio = file_exists('ssh2.sftp://'.$sftp.$caminho_destino);

        if (!$diretorio)
        {
            mkdir('ssh2.sftp://'.$sftp.$caminho_destino);
        }

        //Cria um stream de conexão para abrir o arquivo
        $stream = fopen('ssh2.sftp://'.$sftp.$caminho_destino.$arquivo_destino, 'w');

        //Pega o conteúdo do arquivo
        $conteudo = file_get_contents($caminho_origem.$arquivo_origem);

        //Escreve no arquivo remoto
        fwrite($stream, $conteudo);

        //Fecha o stream
        fclose($stream);
    }

    /**
     * Método linkSimbolico()
     * <pre>
     * Cria um link simbólico de um arquivo em uma máquina remota.
     * </pre>
     * @param $target string Caminho do arquivo original(alvo)
     * @param $link string Caminho do link simbólico
     * @return boolean
     */
    public function linkSimbolico($target, $link)
    {
        //Abre uma sessão de SFTP
        $sftp = ssh2_sftp($this->conexao);

        $link = ssh2_sftp_symlink($sftp, $target, $link);

        if ($link)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Método mkdir()
     * <pre>
     * Cria um diretório na máquina remota se ele não existir.
     * </pre>
     * @param $caminho string Caminho do diretorio a ser criado
     * @param $nome string Nome do diretorio a ser criado
     * @return boolean
     */
    public function mkdir($caminho, $nome)
    {
        //Abre uma sessão de SFTP
        $sftp = ssh2_sftp($this->conexao);

        $diretorio = file_exists("ssh2.sftp://{$caminho}{$nome}");

        if (!$diretorio)
        {
            mkdir("ssh2.sftp://{$sftp}{$caminho}{$nome}");
        }
    }
}

?>
