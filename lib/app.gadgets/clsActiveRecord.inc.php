<?php
/**
 * Barganha - Projeto pessoal de agregador de anúncios
 *
 * @author Alê Borba <ale.alvesborba@gmail.com>
 * @license <a href="http://www.gnu.org/licenses/gpl-3.0.html">GPLv3 - GNU General Public License - Version 3.0</a>
 * @version 0.0.1
 * @package app.gadgets
 */
/**
 * Classe clsActiveRecord
 * <pre>
 * Classe que implementa o Active Record nas ações envolvendo o cliente.
 * </pre>
 * @abstract
 */
abstract class clsActiveRecord
{
    /**
     * @var array
     * <pre>
     * Armazena as propriedades e valores do objeto.
     */
    protected $dados;

    /**
     * Método construtor
     * <pre>
     * Instancia um objeto Active Record. Se passado o ID o objeto já é
     * carregado automaticamente
     * </pre>
     * @param $id integer O ID do objeto
     */
    public function  __construct($id = null)
    {
        //Se o $id for informado
        if ($id)
        {
            //Carrega o objeto
            $objeto = $this->carregar($id);

            if ($objeto)
            {
                $this->fromArray($objeto->toArray());
            }
        }
    }

    /**
     * Método clone
     * <pre>
     * Executado quando um objeto precisar ser clonado.
     * Limpa o ID para que seja criado um novo ID para o clone
     * </pre>
     */
    public function  __clone()
    {
        $id = $this->campoID();
        unset ($this->$id);
    }

    /**
     * Método __get()
     * <pre>
     * Intercepta todas as requisições do tipo get ao objeto e retorna
     * o valor da propriedade especifica.
     * </pre>
     * @access private
     * @param $propriedade string O nome da propriedade que ele vai capiturar
     * @return string
     */
    public function __get($propriedade)
    {
        //Verifica se existe o método get_<propriedade>
        if (method_exists($this, 'get_'.$propriedade))
        {
            //Executa o método
            return call_user_func(array($this, 'get_'.$propriedade));
        }
        else
        {
            //Retorna o valor da propriedade
            return @$this->dados[$propriedade];
        }
    }

    /**
     * Método __set()
     * <pre>
     * Intercepta todas as requisições do tipo set ao objeto e armazena no array
     * de dados.
     * </pre>
     * @access private
     * @param $propriedade string O nome da propriedade que ele vai setar
     * @param $valor string O valor da propriedade que vai ser setada
     */
    public function  __set($propriedade,  $valor)
    {
        //Verifica se existe o método set_<propriedade>
        if (method_exists($this, 'set_'.$propriedade))
        {
            //Executa o método set_<propriedade>
            call_user_func(array($this, 'set_'.$propriedade), $valor);
        }
        else
        {
            //Atribui o valor da propriedade
            $this->dados[$propriedade] = $valor;
        }
    }

    /**
     * Método getTabela
     * <pre>
     * Retorna o nome da tabela usada
     * </pre>
     * @access private
     * @return string
     */
    private function getTabela()
    {
        //Pega o nome da classe
        $classe = strtolower(get_class($this));
        //Substitui o cls do inicio do nome por tbl_ e retorna o valor
        return str_replace('cls', 'tbl_', $classe);
    }

    /**
     * Método campoID
     * <pre>
     * Retorna o nome do campo ID da tabela utilizada
     * </pre>
     * @access protected
     * @return string
     */
    protected function campoID()
    {
        //Pega o nome da classe
        $classe = strtolower(get_class($this));
        //Substitui o cls do inicio do nome por id_ e retorna o valor
        return str_replace('cls', 'id_', $classe);
    }

    /**
     * Método fromArray
     * <pre>
     * Preenche os dados do objeto com um array
     * </pre>
     * @param $dados array Array com os valores das propriedades
     */
    public function fromArray($dados)
    {
        $this->dados = $dados;
    }

    /**
     * Método toArray
     * <pre>
     * Retorna os dados o objeto como array
     * </pre>
     * @return array
     */
    public function toArray()
    {
        return $this->dados;
    }

    /**
     * Método gravar
     * <pre>
     * Armazena o objeto no banco e retorna o numero de linhas afetadas
     * </pre>
     * @return integer
     */
    public function gravar()
    {
        $id = $this->campoID();
        //Verifica se o ID está setado ou se existe no banco
        if (empty ($this->dados["{$id}"]) || (!$this->carregar($this->$id)))
        {
            //Cria uma instrução de Insert
            $sql = new clsInsert();
            //Seta a tabela
            $sql->setTabela($this->getTabela());

            //Percorre os dados do objeto
            foreach ($this->dados as $chave => $valor)
            {
                //Passa os dados do objeto para a instrução SQL
                $sql->setColunaValor($chave, $this->$chave);
            }
        }
        else
        {
            //Cria uma instrução de Update
            $sql = new clsUpdate();
            //Seta a tabela
            $sql->setTabela($this->getTabela());

            //Cria um critério baseado no ID
            $criterio = new clsCriterio();
            $criterio->adicionar(new clsFiltro("{$this->campoID()}", '=', $this->$id));

            //Seta o criterio na instruçao de Update
            $sql->setCriterio($criterio);

            //Percorre todos os dados do objeto
            foreach ($this->dados as $chave => $valor)
            {
                //Tira o ID da lista pois ele não é necessário na instruçao de Update
                if ($chave !== "{$id}")
                {
                    //Passa os dados do objeto para a instrução SQL
                    $sql->setColunaValor($chave, $this->$chave);
                }
            }
        }

        //Pega a transação ativa
        if ($conexao = clsTransacao::get())
        {
            //Grava o log da ação e executa
            clsTransacao::log($sql->getInstrucao());
            $resultado = $conexao->exec($sql->getInstrucao());

            //Carrega o ID na memória
            if (empty ($this->dados["{$this->campoID()}"]) || (!$this->carregar($this->$id)))
            {
                $id = $this->campoID();
                $this->$id = $this->getLast();
            }

            //Retorna o resultado
            return $resultado;
        }
        else
        {
            // Se não for encontrada transação ativa retorna uma exceção
            throw new Exception('Não foi encontrada transação ativa!');
        }
    }

    /**
     * Método carregar
     * <pre>
     * Retorna um objeto da base de dados
     * através do ID e carrega na memória
     * </pre>
     * @param $id integer ID do objeto a ser carregado
     * @return object
     */
    public function carregar($id)
    {
        $id_campo = $this->campoID();
        //Cria uma instrução de Select
        $sql = new clsSelect();
        //Seta a tabela
        $sql->setTabela($this->getTabela());
        $sql->addColuna('*');

        //Cria um critério baseado no ID do objeto
        $criterio = new clsCriterio();
        $criterio->adicionar(new clsFiltro("{$id_campo}", '=', $id));

        //Seta o criterio de seleção
        $sql->setCriterio($criterio);

        //Pega a transação ativa
        if ($conexao = clsTransacao::get())
        {
            //Cria o log e executa o SQL
            clsTransacao::log($sql->getInstrucao());
            $resultado = $conexao->Query($sql->getInstrucao());

            //Confere se obteve algum resultado
            if ($resultado)
            {
                //Retorna os dados na forma de objeto
                $objeto = $resultado->fetchObject(get_class($this));
            }

            return $objeto;
        }
        else
        {
            // Se não encontrar transação ativa retorna uma exeção
            throw new Exception('Não existe transação ativa!');
        }
    }

    /**
     * Método getLast
     * <pre>
     * Pega o último ID incluido no banco
     * </pre>
     * @return integer
     * @access private
     */
    private function getLast()
    {
        // Pega a transação ativa
        if ($conexao = clsTransacao::get())
        {
            // Cria uma instrução de Select
            $sql = new clsSelect();

            $sql->addColuna("max({$this->campoID()}) as id");
            //Seta a tabela
            $sql->setTabela($this->getTabela());

            //Cria um log e executa o SQL
            clsTransacao::log($sql->getInstrucao());
            $resultado = $conexao->Query($sql->getInstrucao());

            //Retorna o resultado da consulta
            $linha = $resultado->fetch();
            return $linha[0];
        }
        else
        {
            // Se não existir transação retorna uma exceção
            throw new Exception('Não existe transação ativa!');
        }
    }

    /**
     * Método delete
     * <pre>
     * Apaga um objeto da base de dados
     * através do ID
     * </pre>
     * @param $id integer ID do objeto a ser carregado
     */
    public function delete($id = null)
    {
        $id_campo = $this->campoID();
        $id = isset ($id) ? $id : $this->$id_campo;
        //Cria uma instrução de Delete
        $sql = new clsDelete();
        //Seta a tabela
        $sql->setTabela($this->getTabela());

        //Cria um critério baseado no ID do objeto
        $criterio = new clsCriterio();
        $criterio->adicionar(new clsFiltro("{$id_campo}", '=', $id));

        //Seta o criterio de seleção
        $sql->setCriterio($criterio);

        //Pega a transação ativa
        if ($conexao = clsTransacao::get())
        {
            //Cria o log e executa o SQL
            clsTransacao::log($sql->getInstrucao());
            $resultado = $conexao->exec($sql->getInstrucao());

            clsTransacao::close();

            //Retorna o resultado
            return $resultado;
        }
        else
        {
            // Se não encontrar transação ativa retorna uma exeção
            throw new Exception('Não existe transação ativa!');
        }
    }

    /**
     * Método adicionar
     * <pre>
     * Método abstrado que força com que todas as classes filhas tenha o seu próprio metodo adicionar
     * com as caracteristicas especificas de cada uma.
     * </pre>
     * @abstract
     */
    abstract function adicionar();
}

?>
